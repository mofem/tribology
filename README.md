[TOC]

## Brief description 
Contact interaction between an elastic solid with given surface roughness and a rigid flat.

***_Assumptions:_*** small rotation, small strain, small tangential displacement (moderate if `my_convect` option is switched on,  see [Input parameters](#markdown-header-input-parameters_1) for more details).

The workflow currently consists of two steps:

1. Creating the contact interface by projecting the surface roughness data on the finite-element mesh (using sequential processing),
2. Solving the contact problem for a given number of load steps (using parallel processing),
3. Postprocessing and visualization.

## 1. Creating rough contact interface 

Executable `create_rough_interface` is designed to work as a **sequential** process and performs the following tasks:

- Project the given surface data on the mesh using the formula `Z -= h * (H + Z) / H`, where `Z` is the vertical coordinate of the mesh node, `h` is the height of the node in the data file and `H` is the vertical size of undeformed mesh,
- Currently the surface data is expected in a single-column text file containing only heights of the points, i.e. a uniform grid is assumed, and the number of nodes on each side is to be provided, see [Input parameters](#markdown-header-input-parameters) for more details.
- Insert contact elements (triangular prisms) between the contact surfaces,
- Partition the elastic solid and the contact interface into given number of parts,
- Add fixed Dirichlet boundary conditions for the rigid flat surface.

### Input parameters

Name | Description | Default value
--- | --- | ---
`my_file` | File name of the input (undeformed) mesh, structured on the surface `z=0` | `mesh.h5m`
`my_data_file` | Name of the surface data file | `data.txt`
`my_out_file` | Name of the output mesh file | `out.h5m`
`my_side_nodes_num` | Number of nodes on each side of the surface `z=0` *(square of this number must be equal to the number of points in the surface data file)* | `1` 
`my_mesh_height` | Vertical size of the input mesh | `1.0`
`my_length_scale` | Scaling parameter for the input mesh | `1.0`
`my_nparts` | Number of parts to partition the mesh to | `1` 

### Command line example

```bash
./create_rough_interface -my_file contact.h5m \
-my_data_file surface_data.txt \ 
-my_out_file contact_48.h5m \
-my_side_nodes_num 257 \
-my_mesh_height 2.0 \ 
-my_length_scale 0.005 \
-my_nparts 48 
```

### Output

Mesh file with the projected roughness and inserted contact elements, partitioned in a given number of parts. 

## 2. Solving the contact problem 

The contact problem for the prepared mesh is solved using the executable `contact_with_rigid_flat`, which is designed for both **sequential** and **parallel** processing. 

### Input parameters

Name | Description | Default value
--- | --- | ---
`my_file` | Input file name | `mesh.h5m`
`my_is_partitioned` | If set to `1` (True), each processor in the parallel environment reads only its part of the mesh | `0`  
`my_order` | Approximation order of the field of spatial positions for the entire mesh | `1`
`my_order_lambda` | Approximation order of the field of contact Lagrange multipliers | `1`
`my_order_contact` | Approximation order of the field of spatial positions for the contact elements and a given number of tetrahedral element layers adjacent to the contact interface | `1`
`my_ho_levels_num` | Number of tetrahedral element layers adjacent to the contact interface with higher order of the field of spatial positions (if `my_order_contact` is higher than `my_order`) | `1` 
`my_load_scale` | Scaling for the external pressure | `1.0`  
`my_step_num` | Number of steps used to achieve the specified load scale (so-called load control). Note that multi-stepping can be particularly important to obtain a solution for highly nonlinear problems | `1` 
`my_cn_value` | Augmentation parameter which affects the convergence and has minimal effect on the solution. Recommended initial value is the Young's modulus of the elastic solid. The optimal value can be found by repetitively increasing/decreasing the initial value by e.g. a factor of 10 | `1.0`
`my_alm_flag` | Defines the choice of the algorithm: `0` (False) - Complementarity function approach, `1` (True) - Augmented Lagrangian method | `1`  
`my_out_reduced_integ` | If set to `1` (True), reduced integration is used for contact elements | `0`  
`my_convect` | If set to `1` (True), moderate relative tangential displacements can be  taken into account | `0`  
`my_out_volume` | If set to `1` (True), solution in the volume is output at each load step. Note that outputting the solution in the volume may increase significantly the run time of the analysis and require large storage for generated files | `0`  
`my_out_skin` | If set to `1` (True), solution on the skin of the elastic solid is output at each load step | `0`  
`my_out_integ_pts` | If set to `1` (True), solution at the contact integration point is printed | `0`  
`my_print_energy` | If set to `1` (True), elastic energy of the whole block is printed to the log file for each load step | `0`  


### Command line example

```bash
mpirun -np 48 ./contact_with_rigid_flat -my_file contact_48.h5m 2>&1 | tee log
```

### Output

- Information about the real active contact area at each load step is printed to the `log` file,
- Material and current coordinates, as well as stress tensor components are output to files `out_N.h5m` and `out_skin_N.h5m` (the latter contain the data only on the skin of the elastic block), where `N` is the number of the step,
- Nodal interpolation of Lagrange multipliers (equivalent to contact pressure) is output to files `out_contact_N.h5m`;
- Values of the Lagrange multipliers and the normal gap at the gauss points of the contact interface are output to files `out_contact_integ_pts_N.h5m`.


## 3. Postprocessing 

First, the output `h5m` files need to be converted to `vtk` format. Note that these files can be converted one by one using the `mbconvert` tool:

```bash
mbconvert out_0.h5m out_0.vtk
mbconvert out_skin_0.h5m out_skin_0.vtk
mbconvert out_contact_0.h5m out_contact_0.vtk
mbconvert out_contact_integ_pts_0.h5m out_contact_integ_pts_0.vtk
```
or all at once by using the multiprocessing script `convert.py`:

```bash
$USER_MODULES/tools/convert.py -np 2 out*
```

The obtained `vtk` files can be viewed in *Paraview*, in particular:

- Files `out_N.vtk` and `out_skin_N.vtk` contain the stress tensor components (tag `SPATIAL_POSITION_PIOLA1_STRESS`), as well as material coordinates (tag `MESH_NODE_POSITIONS`) and current coordinates (tag `SPATIAL_POSITION`), which can be used to compute the displacement field with the *Calculator* filter as `DISPLACEMENT=SPATIAL_POSITION-MESH_NODE_POSITIONS`,
- Files `out_contact_N.vtk` contains the nodal interpolation of the Lagrange multipliers equivalent to the contact pressure (tag `LAGMULT`),
- Files `out_contact_integ_pts_N.vtk` contains values of Lagrange multipliers (tag `LAGMULT`) and the normal gap (tag `GAP`) at Gauss points of the contact interface. Note that the _Point Gaussian_ representation or alternatively the _Glyph_ filter should be used for their visualisation in *Paraview*.


