/** \file contact_with_rigid_flat.cpp
 * \example contact_with_rigid_flat.cpp
 *
 * Implementation of simple contact (matching meshes)
 * between a solid with a rough surface and a rigid flat
 *
 **/

/* MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>.
 */

#include <BasicFiniteElements.hpp>
#include <Hooke.hpp>

using namespace std;
using namespace MoFEM;

using BlockData = NonlinearElasticElement::BlockData;
using VolSideFe = VolumeElementForcesAndSourcesCoreOnSide;

/// Set integration rule
struct VolRule {
  int operator()(int, int, int order) const { return 2 * (order - 1); }
};
struct ContactRule {
  int operator()(int, int, int order) const { return order; }
};

static char help[] = "\n";
double SimpleContactProblem::LoadScale::lAmbda = 1;

int main(int argc, char *argv[]) {

  const string default_options = "-ksp_type fgmres \n"
                                 "-pc_type lu \n"
                                 "-pc_factor_mat_solver_package mumps \n"
                                 "-snes_type newtonls \n"
                                 "-snes_linesearch_type basic \n"
                                 "-snes_max_it 10 \n"
                                 "-snes_atol 1e-8 \n"
                                 "-snes_rtol 1e-8 \n"
                                 "-my_order 1 \n"
                                 "-my_order_lambda 1 \n"
                                 "-my_cn_value 1. \n"
                                 "-my_load_scale 1. \n"
                                 "-my_is_newton_cotes 0 \n"
                                 "-my_is_test 0 \n"
                                 "-my_integ_levels_num 0 \n";

  string param_file = "param_file.petsc";
  if (!static_cast<bool>(ifstream(param_file))) {
    std::ofstream file(param_file.c_str(), std::ios::ate);
    if (file.is_open()) {
      file << default_options;
      file.close();
    }
  }

  // Initialize MoFEM
  MoFEM::Core::Initialize(&argc, &argv, param_file.c_str(), help);

  // Create mesh database
  moab::Core mb_instance;              // create database
  moab::Interface &moab = mb_instance; // create interface to database

  try {
    PetscBool flg_file;

    char mesh_file_name[255];
    PetscInt order = 1;
    PetscInt order_lambda = 1;
    PetscInt order_contact = 1;
    PetscInt nb_ho_levels = 0;
    PetscReal r_value = 1.;
    PetscReal cn_value = 1.;
    PetscReal load_scale = 1.;
    PetscInt nb_sub_steps = 1;
    PetscBool is_partitioned = PETSC_FALSE;
    PetscBool is_newton_cotes = PETSC_FALSE;
    PetscBool is_test = PETSC_FALSE;
    PetscBool convect_pts = PETSC_FALSE;
    PetscBool alm_flag = PETSC_TRUE;
    PetscBool print_energy = PETSC_FALSE;
    PetscBool out_volume = PETSC_FALSE;
    PetscBool out_skin = PETSC_FALSE;
    PetscBool out_integ_pts = PETSC_FALSE;
    PetscBool use_reduced_integ = PETSC_FALSE;
    PetscBool use_solution_update = PETSC_FALSE;
    PetscBool ho_adj_tets = PETSC_FALSE;

    CHKERR PetscOptionsBegin(PETSC_COMM_WORLD, "", "Elastic Config", "none");

    CHKERR PetscOptionsString("-my_file", "mesh file name", "", "mesh.h5m",
                              mesh_file_name, 255, &flg_file);

    CHKERR PetscOptionsInt("-my_order",
                           "approximation order of spatial positions", "", 1,
                           &order, PETSC_NULL);
    CHKERR PetscOptionsInt("-my_order_lambda",
                           "approximation order of Lagrange multipliers", "", 1,
                           &order_lambda, PETSC_NULL);

    CHKERR PetscOptionsInt(
        "-my_order_contact",
        "approximation order of spatial positions for ho levels", "", 1,
        &order_contact, PETSC_NULL);
    CHKERR PetscOptionsInt("-my_ho_levels_num", "number of higher order levels",
                           "", 0, &nb_ho_levels, PETSC_NULL);

    CHKERR PetscOptionsInt("-my_step_num", "number of steps", "", nb_sub_steps,
                           &nb_sub_steps, PETSC_NULL);

    CHKERR PetscOptionsBool(
        "-my_is_partitioned",
        "set if mesh is partitioned (each process keeps only part of the mesh)",
        "", PETSC_FALSE, &is_partitioned, PETSC_NULL);

    CHKERR PetscOptionsReal("-my_cn_value", "default regularisation cn value",
                            "", 1., &cn_value, PETSC_NULL);
    CHKERR PetscOptionsReal("-my_load_scale", "scale of external load", "", 1.,
                            &load_scale, PETSC_NULL);

    CHKERR PetscOptionsBool("-my_is_newton_cotes",
                            "set if Newton-Cotes quadrature rules are used", "",
                            PETSC_FALSE, &is_newton_cotes, PETSC_NULL);
    CHKERR PetscOptionsBool("-my_is_test", "set if run as test", "",
                            PETSC_FALSE, &is_test, PETSC_NULL);
    CHKERR PetscOptionsBool("-my_convect", "set to convect integration pts", "",
                            PETSC_FALSE, &convect_pts, PETSC_NULL);
    CHKERR PetscOptionsBool("-my_alm_flag", "set to use ALM", "", PETSC_TRUE,
                            &alm_flag, PETSC_NULL);
    CHKERR PetscOptionsBool("-my_print_energy", "set to print strain energy",
                            "", PETSC_FALSE, &print_energy, PETSC_NULL);

    CHKERR PetscOptionsBool("-my_out_volume",
                            "set to output solution in the volume", "",
                            PETSC_FALSE, &out_volume, PETSC_NULL);
    CHKERR PetscOptionsBool("-my_out_skin",
                            "set to output solution on the skin", "",
                            PETSC_FALSE, &out_skin, PETSC_NULL);

    CHKERR PetscOptionsBool(
        "-my_out_integ_pts",
        "set to output solution at the contact integration points", "",
        PETSC_FALSE, &out_integ_pts, PETSC_NULL);

    CHKERR PetscOptionsBool("-my_reduced_integ",
                            "set to use reduced integration for contact", "",
                            PETSC_FALSE, &use_reduced_integ, PETSC_NULL);
    CHKERR PetscOptionsBool(
        "-my_solution_update",
        "set to use solution update to speed-up convergence", "", PETSC_FALSE,
        &use_solution_update, PETSC_NULL);
    CHKERR PetscOptionsBool("-my_ho_adj_tets",
                            "set higher order to tets adjacent to contact tris",
                            "", PETSC_FALSE, &ho_adj_tets, PETSC_NULL);

    ierr = PetscOptionsEnd();
    CHKERRQ(ierr);

    // Check if mesh file was provided
    if (flg_file != PETSC_TRUE) {
      SETERRQ(PETSC_COMM_SELF, 1, "*** ERROR -my_file (MESH FILE NEEDED)");
    }

    if (is_partitioned == PETSC_TRUE) {
      // Read mesh to MOAB
      const char *option;
      option = "PARALLEL=READ_PART;"
               "PARALLEL_RESOLVE_SHARED_ENTS;"
               "PARTITION=PARALLEL_PARTITION;";
      CHKERR moab.load_file(mesh_file_name, 0, option);
    } else {
      const char *option;
      option = "";
      CHKERR moab.load_file(mesh_file_name, 0, option);
    }

    ParallelComm *pcomm = ParallelComm::get_pcomm(&moab, MYPCOMM_INDEX);
    if (pcomm == NULL)
      pcomm = new ParallelComm(&moab, PETSC_COMM_WORLD);

    // Create MoFEM database and link it to MoAB
    MoFEM::Core core(moab);
    MoFEM::Interface &m_field = core;

    MeshsetsManager *mmanager_ptr;
    CHKERR m_field.getInterface(mmanager_ptr);

    CHKERR mmanager_ptr->printDisplacementSet();
    CHKERR mmanager_ptr->printMaterialsSet();
    CHKERR mmanager_ptr->printPressureSet();

    BitRefLevel bit_level0;
    bit_level0.set(0);
    CHKERR m_field.getInterface<BitRefManager>()->setBitRefLevelByDim(
        0, 3, bit_level0);

    Range contact_prisms, bot_tris, top_tris;
    CHKERR m_field.getInterface<BitRefManager>()->getEntitiesByTypeAndRefLevel(
        bit_level0, BitRefLevel().set(), MBPRISM, contact_prisms);
    CHKERR m_field.getInterface<CommInterface>()->synchroniseEntities(
        contact_prisms);

    EntityHandle tri;
    for (auto p : contact_prisms) {
      CHKERR moab.side_element(p, 2, 3, tri);
      bot_tris.insert(tri);
      CHKERR moab.side_element(p, 2, 4, tri);
      top_tris.insert(tri);
    }
    CHKERR m_field.getInterface<CommInterface>()->synchroniseEntities(bot_tris);
    CHKERR m_field.getInterface<CommInterface>()->synchroniseEntities(top_tris);

    CHKERR m_field.add_field("SPATIAL_POSITION", H1, AINSWORTH_LEGENDRE_BASE, 3,
                             MB_TAG_SPARSE, MF_ZERO);
    CHKERR m_field.add_field("MESH_NODE_POSITIONS", H1, AINSWORTH_LEGENDRE_BASE,
                             3, MB_TAG_SPARSE, MF_ZERO);
    CHKERR m_field.add_field("LAGMULT", H1, AINSWORTH_LEGENDRE_BASE, 1,
                             MB_TAG_SPARSE, MF_ZERO);

    CHKERR m_field.add_ents_to_field_by_type(0, MBTET, "MESH_NODE_POSITIONS");
    CHKERR m_field.add_ents_to_field_by_type(top_tris, MBTRI,
                                             "MESH_NODE_POSITIONS");
    CHKERR m_field.set_field_order(0, MBVERTEX, "MESH_NODE_POSITIONS", 1);

    // Declare problem add entities (by tets) to the field
    CHKERR m_field.add_ents_to_field_by_type(0, MBTET, "SPATIAL_POSITION");
    CHKERR m_field.add_ents_to_field_by_type(top_tris, MBTRI,
                                             "SPATIAL_POSITION");
    CHKERR m_field.set_field_order(0, MBTET, "SPATIAL_POSITION", order);
    CHKERR m_field.set_field_order(0, MBTRI, "SPATIAL_POSITION", order);
    CHKERR m_field.set_field_order(0, MBEDGE, "SPATIAL_POSITION", order);
    CHKERR m_field.set_field_order(0, MBVERTEX, "SPATIAL_POSITION", 1);

    CHKERR m_field.add_ents_to_field_by_type(top_tris, MBTRI, "LAGMULT");
    CHKERR m_field.set_field_order(0, MBTRI, "LAGMULT", order_lambda);
    CHKERR m_field.set_field_order(0, MBEDGE, "LAGMULT", order_lambda);
    CHKERR m_field.set_field_order(0, MBVERTEX, "LAGMULT", 1);

    CHKERR m_field.getInterface<CommInterface>()->synchroniseFieldEntities(
        "SPATIAL_POSITION");
    CHKERR m_field.getInterface<CommInterface>()->synchroniseFieldEntities(
        "MESH_NODE_POSITIONS");
    CHKERR m_field.getInterface<CommInterface>()->synchroniseFieldEntities(
        "LAGMULT");

    if (order_contact > order) {
      Range contact_tris, contact_edges;
      contact_tris.merge(top_tris);
      contact_tris.merge(bot_tris);
      CHKERR moab.get_adjacencies(contact_tris, 1, false, contact_edges,
                                  moab::Interface::UNION);
      Range ho_ents;
      ho_ents.merge(contact_tris);
      ho_ents.merge(contact_edges);
      if (!ho_adj_tets) {
        for (int ll = 0; ll < nb_ho_levels; ll++) {
          Range ents, verts, tets;
          CHKERR moab.get_connectivity(ho_ents, verts, true);
          CHKERR moab.get_adjacencies(verts, 3, false, tets,
                                      moab::Interface::UNION);
          tets = tets.subset_by_type(MBTET);
          for (auto d : {1, 2}) {
            CHKERR moab.get_adjacencies(tets, d, false, ents,
                                        moab::Interface::UNION);
          }
          ho_ents.merge(ents);
          ho_ents.merge(tets);
        }
      } else {
        Range tets, ents;
        CHKERR moab.get_adjacencies(contact_tris, 3, false, tets,
                                    moab::Interface::UNION);
        tets = tets.subset_by_type(MBTET);                            
        for (auto d : {1, 2}) {
          CHKERR moab.get_adjacencies(tets, d, false, ents,
                                      moab::Interface::UNION);
        }
        ho_ents.merge(ents);
        ho_ents.merge(tets);
      }

      CHKERR m_field.getInterface<CommInterface>()->synchroniseEntities(
          ho_ents);
      CHKERR m_field.set_field_order(ho_ents, "SPATIAL_POSITION",
                                     order_contact);
    }

    // build field
    CHKERR m_field.build_fields();

    // Projection on "x" field
    {
      Projection10NodeCoordsOnField ent_method(m_field, "SPATIAL_POSITION");
      CHKERR m_field.loop_dofs("SPATIAL_POSITION", ent_method);
    }
    // Projection on "X" field
    {
      Projection10NodeCoordsOnField ent_method(m_field, "MESH_NODE_POSITIONS");
      CHKERR m_field.loop_dofs("MESH_NODE_POSITIONS", ent_method);
    }

    boost::shared_ptr<std::map<int, BlockData>> block_sets_ptr =
        boost::make_shared<std::map<int, BlockData>>();
    CHKERR HookeElement::setBlocks(m_field, block_sets_ptr);

    boost::shared_ptr<ForcesAndSourcesCore> fe_elastic_lhs_ptr(
        new VolumeElementForcesAndSourcesCore(m_field));
    boost::shared_ptr<ForcesAndSourcesCore> fe_elastic_rhs_ptr(
        new VolumeElementForcesAndSourcesCore(m_field));
    fe_elastic_lhs_ptr->getRuleHook = VolRule();
    fe_elastic_rhs_ptr->getRuleHook = VolRule();

    CHKERR HookeElement::addElasticElement(m_field, block_sets_ptr, "ELASTIC",
                                           "SPATIAL_POSITION",
                                           "MESH_NODE_POSITIONS", false);

    auto data_hooke_element_at_pts =
        boost::make_shared<HookeElement::DataAtIntegrationPts>();
    CHKERR HookeElement::setOperators(fe_elastic_lhs_ptr, fe_elastic_rhs_ptr,
                                      block_sets_ptr, "SPATIAL_POSITION",
                                      "MESH_NODE_POSITIONS", false, false,
                                      MBTET, data_hooke_element_at_pts);

    Range skin_tris;
    for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field, BLOCKSET, bit)) {
      if (bit->getName().compare(0, 4, "SKIN") == 0) {
        CHKERR moab.get_entities_by_dimension(bit->getMeshset(), 2, skin_tris,
                                              true);
      }
    }

    CHKERR m_field.add_finite_element("SKIN", MF_ZERO);
    CHKERR m_field.add_ents_to_finite_element_by_type(skin_tris, MBTRI, "SKIN");
    CHKERR m_field.modify_finite_element_add_field_row("SKIN",
                                                       "SPATIAL_POSITION");
    CHKERR m_field.modify_finite_element_add_field_col("SKIN",
                                                       "SPATIAL_POSITION");
    CHKERR m_field.modify_finite_element_add_field_data("SKIN",
                                                        "SPATIAL_POSITION");
    CHKERR m_field.modify_finite_element_add_field_data("SKIN",
                                                        "MESH_NODE_POSITIONS");

    auto make_contact_element = [&]() {
      auto contact_element =
          boost::make_shared<SimpleContactProblem::SimpleContactElement>(
              m_field);

      if (use_reduced_integ) {
        contact_element->getRuleHook = ContactRule();
      }

      return contact_element;
    };

    auto make_convective_master_element = [&]() {
      return boost::make_shared<
          SimpleContactProblem::ConvectMasterContactElement>(
          m_field, "SPATIAL_POSITION", "MESH_NODE_POSITIONS");
    };

    auto make_convective_slave_element = [&]() {
      return boost::make_shared<
          SimpleContactProblem::ConvectSlaveContactElement>(
          m_field, "SPATIAL_POSITION", "MESH_NODE_POSITIONS");
    };

    auto make_contact_common_data = [&]() {
      return boost::make_shared<SimpleContactProblem::CommonDataSimpleContact>(
          m_field);
    };

    auto get_contact_rhs = [&](auto contact_problem, auto make_element,
                               bool alm_flag = false) {
      auto fe_rhs_simple_contact = make_element();
      auto common_data_simple_contact = make_contact_common_data();
      fe_rhs_simple_contact->contactStateVec =
          common_data_simple_contact->gaussPtsStateVec;
      contact_problem->setContactOperatorsRhs(
          fe_rhs_simple_contact, common_data_simple_contact, "SPATIAL_POSITION",
          "LAGMULT", alm_flag);
      return fe_rhs_simple_contact;
    };

    auto get_master_traction_rhs = [&](auto contact_problem, auto make_element,
                                       bool alm_flag = false) {
      auto fe_rhs_simple_contact = make_element();
      auto common_data_simple_contact = make_contact_common_data();
      contact_problem->setMasterForceOperatorsRhs(
          fe_rhs_simple_contact, common_data_simple_contact, "SPATIAL_POSITION",
          "LAGMULT", alm_flag);
      return fe_rhs_simple_contact;
    };

    auto get_master_traction_lhs = [&](auto contact_problem, auto make_element,
                                       bool alm_flag = false) {
      auto fe_lhs_simple_contact = make_element();
      auto common_data_simple_contact = make_contact_common_data();
      contact_problem->setMasterForceOperatorsLhs(
          fe_lhs_simple_contact, common_data_simple_contact, "SPATIAL_POSITION",
          "LAGMULT", alm_flag);
      return fe_lhs_simple_contact;
    };

    auto get_master_contact_lhs = [&](auto contact_problem, auto make_element,
                                      bool alm_flag = false) {
      auto fe_lhs_simple_contact = make_element();
      auto common_data_simple_contact = make_contact_common_data();
      contact_problem->setContactOperatorsLhs(
          fe_lhs_simple_contact, common_data_simple_contact, "SPATIAL_POSITION",
          "LAGMULT", alm_flag);
      return fe_lhs_simple_contact;
    };

    auto cn_value_ptr = boost::make_shared<double>(cn_value);
    auto contact_problem = boost::make_shared<SimpleContactProblem>(
        m_field, cn_value_ptr, is_newton_cotes);

    // add fields to the global matrix by adding the element
    contact_problem->addContactElement("CONTACT_ELEM", "SPATIAL_POSITION",
                                       "LAGMULT", contact_prisms);
    contact_problem->addPostProcContactElement("CONTACT_POST_PROC",
                                               "SPATIAL_POSITION", "LAGMULT",
                                               "MESH_NODE_POSITIONS", top_tris);

    CHKERR MetaNeumannForces::addNeumannBCElements(m_field, "SPATIAL_POSITION");

    // Add spring boundary condition applied on surfaces.
    CHKERR MetaSpringBC::addSpringElements(m_field, "SPATIAL_POSITION",
                                           "MESH_NODE_POSITIONS");

    // build finite elemnts
    CHKERR m_field.build_finite_elements();

    // build adjacencies
    CHKERR m_field.build_adjacencies(bit_level0);

    // define problems
    CHKERR m_field.add_problem("CONTACT_PROB");

    // set refinement level for problem
    CHKERR m_field.modify_problem_ref_level_add_bit("CONTACT_PROB", bit_level0);

    DMType dm_name = "DMMOFEM";
    CHKERR DMRegister_MoFEM(dm_name);

    SmartPetscObj<DM> dm;
    dm = createSmartDM(m_field.get_comm(), dm_name);

    // create dm instance
    CHKERR DMSetType(dm, dm_name);

    // set dm datastruture which created mofem datastructures
    CHKERR DMMoFEMCreateMoFEM(dm, &m_field, "CONTACT_PROB", bit_level0);
    CHKERR DMSetFromOptions(dm);
    CHKERR DMMoFEMSetIsPartitioned(dm, is_partitioned);
    // add elements to dm
    CHKERR DMMoFEMAddElement(dm, "CONTACT_ELEM");
    CHKERR DMMoFEMAddElement(dm, "ELASTIC");
    CHKERR DMMoFEMAddElement(dm, "PRESSURE_FE");
    CHKERR DMMoFEMAddElement(dm, "SPRING");
    CHKERR DMMoFEMAddElement(dm, "CONTACT_POST_PROC");
    CHKERR DMMoFEMAddElement(dm, "SKIN");

    m_field.getInterface<ProblemsManager>()->buildProblemFromFields =
        PETSC_TRUE;

    CHKERR DMSetUp(dm);

    // Vector of DOFs and the RHS
    auto D = smartCreateDMVector(dm);
    auto F = smartVectorDuplicate(D);
    auto DU = smartVectorDuplicate(D);

    // Stiffness matrix
    auto Aij = smartCreateDMMatrix(dm);

    CHKERR VecZeroEntries(D);
    CHKERR DMoFEMMeshToLocalVector(dm, D, INSERT_VALUES, SCATTER_FORWARD);
    CHKERR VecGhostUpdateBegin(D, INSERT_VALUES, SCATTER_FORWARD);
    CHKERR VecGhostUpdateEnd(D, INSERT_VALUES, SCATTER_FORWARD);

    CHKERR VecZeroEntries(F);
    CHKERR VecGhostUpdateBegin(F, INSERT_VALUES, SCATTER_FORWARD);
    CHKERR VecGhostUpdateEnd(F, INSERT_VALUES, SCATTER_FORWARD);

    CHKERR MatSetOption(Aij, MAT_SPD, PETSC_TRUE);
    CHKERR MatZeroEntries(Aij);

    fe_elastic_rhs_ptr->snes_f = F;
    fe_elastic_lhs_ptr->snes_B = Aij;

    // Dirichlet BC
    boost::shared_ptr<FEMethod> dirichlet_bc_ptr =
        boost::shared_ptr<FEMethod>(new DirichletSpatialPositionsBc(
            m_field, "SPATIAL_POSITION", Aij, D, F));

    // dirichlet_bc_ptr->snes_ctx = SnesMethod::CTX_SNESNONE;
    dirichlet_bc_ptr->snes_x = D;

    // Assemble pressure and traction forces
    boost::ptr_map<std::string, NeumannForcesSurface> neumann_forces;
    CHKERR MetaNeumannForces::setMomentumFluxOperators(
        m_field, neumann_forces, NULL, "SPATIAL_POSITION");
    boost::ptr_map<std::string, NeumannForcesSurface>::iterator mit =
        neumann_forces.begin();
    for (; mit != neumann_forces.end(); mit++) {
      mit->second->methodsOp.push_back(new SimpleContactProblem::LoadScale());
      CHKERR DMMoFEMSNESSetFunction(dm, mit->first.c_str(),
                                    &mit->second->getLoopFe(), NULL, NULL);
    }

    // Implementation of spring element
    // Create new instances of face elements for springs
    boost::shared_ptr<FaceElementForcesAndSourcesCore> fe_spring_lhs_ptr(
        new FaceElementForcesAndSourcesCore(m_field));
    boost::shared_ptr<FaceElementForcesAndSourcesCore> fe_spring_rhs_ptr(
        new FaceElementForcesAndSourcesCore(m_field));

    CHKERR MetaSpringBC::setSpringOperators(
        m_field, fe_spring_lhs_ptr, fe_spring_rhs_ptr, "SPATIAL_POSITION",
        "MESH_NODE_POSITIONS");

    CHKERR DMoFEMPreProcessFiniteElements(dm, dirichlet_bc_ptr.get());
    CHKERR VecGhostUpdateBegin(D, INSERT_VALUES, SCATTER_FORWARD);
    CHKERR VecGhostUpdateEnd(D, INSERT_VALUES, SCATTER_FORWARD);
    CHKERR DMoFEMMeshToLocalVector(dm, D, INSERT_VALUES, SCATTER_REVERSE);
    CHKERR DMMoFEMSNESSetFunction(dm, DM_NO_ELEMENT, NULL,
                                  dirichlet_bc_ptr.get(), NULL);

    if (convect_pts == PETSC_TRUE) {
      CHKERR DMMoFEMSNESSetFunction(
          dm, "CONTACT_ELEM",
          get_contact_rhs(contact_problem, make_convective_master_element),
          PETSC_NULL, PETSC_NULL);
      CHKERR DMMoFEMSNESSetFunction(
          dm, "CONTACT_ELEM",
          get_master_traction_rhs(contact_problem,
                                  make_convective_slave_element),
          PETSC_NULL, PETSC_NULL);
    } else {
      // if (!integ_levels_num) {
      CHKERR DMMoFEMSNESSetFunction(
          dm, "CONTACT_ELEM",
          get_contact_rhs(contact_problem, make_contact_element, alm_flag),
          PETSC_NULL, PETSC_NULL);
      CHKERR DMMoFEMSNESSetFunction(
          dm, "CONTACT_ELEM",
          get_master_traction_rhs(contact_problem, make_contact_element,
                                  alm_flag),
          PETSC_NULL, PETSC_NULL);
    }
    CHKERR DMMoFEMSNESSetFunction(dm, "ELASTIC", fe_elastic_rhs_ptr, PETSC_NULL,
                                  PETSC_NULL);
    CHKERR DMMoFEMSNESSetFunction(dm, "SPRING", fe_spring_rhs_ptr, PETSC_NULL,
                                  PETSC_NULL);
    CHKERR DMMoFEMSNESSetFunction(dm, DM_NO_ELEMENT, NULL, NULL,
                                  dirichlet_bc_ptr.get());

    boost::shared_ptr<FEMethod> fe_null;
    CHKERR DMMoFEMSNESSetJacobian(dm, DM_NO_ELEMENT, fe_null, dirichlet_bc_ptr,
                                  fe_null);
    if (convect_pts == PETSC_TRUE) {
      CHKERR DMMoFEMSNESSetJacobian(
          dm, "CONTACT_ELEM",
          get_master_contact_lhs(contact_problem,
                                 make_convective_master_element),
          NULL, NULL);
      CHKERR DMMoFEMSNESSetJacobian(
          dm, "CONTACT_ELEM",
          get_master_traction_lhs(contact_problem,
                                  make_convective_slave_element),
          NULL, NULL);
    } else {
      CHKERR DMMoFEMSNESSetJacobian(dm, "CONTACT_ELEM",
                                    get_master_contact_lhs(contact_problem,
                                                           make_contact_element,
                                                           alm_flag),
                                    NULL, NULL);
      CHKERR DMMoFEMSNESSetJacobian(
          dm, "CONTACT_ELEM",
          get_master_traction_lhs(contact_problem, make_contact_element,
                                  alm_flag),
          NULL, NULL);
    }
    CHKERR DMMoFEMSNESSetJacobian(dm, "ELASTIC", fe_elastic_lhs_ptr, NULL,
                                  NULL);
    CHKERR DMMoFEMSNESSetJacobian(dm, "SPRING", fe_spring_lhs_ptr, NULL, NULL);
    CHKERR DMMoFEMSNESSetJacobian(dm, DM_NO_ELEMENT, fe_null, fe_null,
                                  dirichlet_bc_ptr);

    if (is_test == PETSC_TRUE) {
      char testing_options[] = "-ksp_type fgmres "
                               "-pc_type lu "
                               "-pc_factor_mat_solver_package mumps "
                               "-snes_type newtonls "
                               "-snes_linesearch_type basic "
                               "-snes_max_it 10 "
                               "-snes_atol 1e-8 "
                               "-snes_rtol 1e-8 ";
      CHKERR PetscOptionsInsertString(NULL, testing_options);
    }

    auto snes = MoFEM::createSNES(m_field.get_comm());
    CHKERR SNESSetDM(snes, dm);
    SNESConvergedReason snes_reason;
    SnesCtx *snes_ctx;
    // create snes nonlinear solver
    {
      CHKERR SNESSetDM(snes, dm);
      CHKERR DMMoFEMGetSnesCtx(dm, &snes_ctx);
      CHKERR SNESSetFunction(snes, F, SnesRhs, snes_ctx);
      CHKERR SNESSetJacobian(snes, Aij, Aij, SnesMat, snes_ctx);
      CHKERR SNESSetFromOptions(snes);

      KSP ksp;
      CHKERR SNESGetKSP(snes, &ksp);
      PC pc;
      CHKERR KSPGetPC(ksp, &pc);
      PetscBool is_pcfs = PETSC_FALSE;
      PetscObjectTypeCompare((PetscObject)pc, PCFIELDSPLIT, &is_pcfs);
      // Set up FIELDSPLIT
      // Only is user set -pc_type fieldsplit
      if (is_pcfs == PETSC_TRUE) {
        IS is_spatpos, is_lagmult;
        const MoFEM::Problem *problem_ptr;
        CHKERR DMMoFEMGetProblemPtr(dm, &problem_ptr);
        CHKERR m_field.getInterface<ISManager>()->isCreateProblemFieldAndRank(
            problem_ptr->getName(), ROW, "SPATIAL_POSITION", 0, 3, &is_spatpos);
        CHKERR m_field.getInterface<ISManager>()->isCreateProblemFieldAndRank(
            problem_ptr->getName(), ROW, "LAGMULT", 0, 1, &is_lagmult);

        // CHKERR ISView(is_spatpos, PETSC_VIEWER_STDOUT_SELF);
        // CHKERR ISView(is_lagmult, PETSC_VIEWER_STDOUT_SELF);

        CHKERR ISSortRemoveDups(is_spatpos);
        CHKERR ISSortRemoveDups(is_lagmult);

        // CHKERR ISSort(is_spatpos);
        // CHKERR ISSort(is_lagmult);

        CHKERR PCFieldSplitSetIS(pc, "spatpos", is_spatpos);
        CHKERR PCFieldSplitSetIS(pc, "lagmult", is_lagmult);

        CHKERR ISDestroy(&is_spatpos);
        CHKERR ISDestroy(&is_lagmult);
      }
    }

    /// Post proc in the volume
    PostProcVolumeOnRefinedMesh post_proc(m_field);
    // Add operators to the elements, starting with some generic
    CHKERR post_proc.generateReferenceElementMesh();
    CHKERR post_proc.addFieldValuesPostProc("SPATIAL_POSITION");
    CHKERR post_proc.addFieldValuesPostProc("MESH_NODE_POSITIONS");
    CHKERR post_proc.addFieldValuesGradientPostProc("SPATIAL_POSITION");
    post_proc.getOpPtrVector().push_back(new PostProcHookStress(
        m_field, post_proc.postProcMesh, post_proc.mapGaussPts,
        "SPATIAL_POSITION", post_proc.commonData, block_sets_ptr.get(), false));

    /// Post proc on the skin
    PostProcFaceOnRefinedMesh post_proc_skin(m_field);
    CHKERR post_proc_skin.generateReferenceElementMesh();
    CHKERR post_proc_skin.addFieldValuesPostProc("SPATIAL_POSITION");
    CHKERR post_proc_skin.addFieldValuesPostProc("MESH_NODE_POSITIONS");

    struct OpGetFieldGradientValuesOnSkin
        : public FaceElementForcesAndSourcesCore::UserDataOperator {

      const std::string feVolName;
      boost::shared_ptr<VolSideFe> sideOpFe;

      OpGetFieldGradientValuesOnSkin(const std::string field_name,
                                     const std::string vol_fe_name,
                                     boost::shared_ptr<VolSideFe> side_fe)
          : FaceElementForcesAndSourcesCore::UserDataOperator(
                field_name, UserDataOperator::OPROW),
            feVolName(vol_fe_name), sideOpFe(side_fe) {}

      MoFEMErrorCode doWork(int side, EntityType type,
                            DataForcesAndSourcesCore::EntData &data) {
        MoFEMFunctionBegin;
        if (type != MBVERTEX)
          MoFEMFunctionReturnHot(0);
        CHKERR loopSideVolumes(feVolName, *sideOpFe);
        MoFEMFunctionReturn(0);
      }
    };

    boost::shared_ptr<VolSideFe> my_vol_side_fe_ptr =
        boost::make_shared<VolSideFe>(m_field);
    my_vol_side_fe_ptr->getOpPtrVector().push_back(
        new OpCalculateVectorFieldGradient<3, 3>(
            "SPATIAL_POSITION", data_hooke_element_at_pts->hMat));
    my_vol_side_fe_ptr->getOpPtrVector().push_back(
        new OpCalculateVectorFieldGradient<3, 3>(
            "MESH_NODE_POSITIONS", data_hooke_element_at_pts->HMat));

    post_proc_skin.getOpPtrVector().push_back(
        new OpGetFieldGradientValuesOnSkin("SPATIAL_POSITION", "ELASTIC",
                                           my_vol_side_fe_ptr));
    post_proc_skin.getOpPtrVector().push_back(
        new HookeElement::OpPostProcHookeElement<
            FaceElementForcesAndSourcesCore>(
            "SPATIAL_POSITION", data_hooke_element_at_pts,
            *block_sets_ptr.get(), post_proc_skin.postProcMesh,
            post_proc_skin.mapGaussPts, false, false));

    /// Post proc on the contact surface (rigid flat side)
    boost::shared_ptr<PostProcFaceOnRefinedMesh> post_proc_contact_ptr(
        new PostProcFaceOnRefinedMesh(m_field));
    CHKERR post_proc_contact_ptr->generateReferenceElementMesh();
    CHKERR post_proc_contact_ptr->addFieldValuesPostProc("LAGMULT");

    for (int ss = 0; ss != nb_sub_steps; ++ss) {

      if (use_solution_update && ss > 0) {
        CHKERR SNESGetSolutionUpdate(snes, (Vec*)&DU);
        CHKERR VecGhostUpdateBegin(DU, INSERT_VALUES, SCATTER_FORWARD);
        CHKERR VecGhostUpdateEnd(DU, INSERT_VALUES, SCATTER_FORWARD);
        CHKERR VecAXPY(D, 1.0, DU);
        CHKERR VecGhostUpdateBegin(D, INSERT_VALUES, SCATTER_FORWARD);
        CHKERR VecGhostUpdateEnd(D, INSERT_VALUES, SCATTER_FORWARD);
      }

      SimpleContactProblem::LoadScale::lAmbda =
          load_scale * (ss + 1.0) / nb_sub_steps;

      CHKERR PetscPrintf(PETSC_COMM_WORLD, "\nLoad step: %d out of %d\n",
                         ss + 1, nb_sub_steps);
      CHKERR PetscPrintf(PETSC_COMM_WORLD, "Load scale: %6.4e\n",
                         SimpleContactProblem::LoadScale::lAmbda);

      CHKERR SNESSolve(snes, PETSC_NULL, D);

      // save on mesh
      CHKERR VecGhostUpdateBegin(D, INSERT_VALUES, SCATTER_FORWARD);
      CHKERR VecGhostUpdateEnd(D, INSERT_VALUES, SCATTER_FORWARD);
      CHKERR DMoFEMMeshToLocalVector(dm, D, INSERT_VALUES, SCATTER_REVERSE);

      // moab_instance
      moab::Core mb_post;                   // create database
      moab::Interface &moab_proc = mb_post; // create interface to database

      auto common_data_simple_contact = make_contact_common_data();

      boost::shared_ptr<SimpleContactProblem::SimpleContactElement>
          fe_post_proc_simple_contact;
      if (convect_pts == PETSC_TRUE) {
        fe_post_proc_simple_contact = make_convective_master_element();
      } else {
        fe_post_proc_simple_contact = make_contact_element();
      }

      contact_problem->setContactOperatorsForPostProc(
          fe_post_proc_simple_contact, common_data_simple_contact, m_field,
          "SPATIAL_POSITION", "LAGMULT", mb_post, alm_flag);

      mb_post.delete_mesh();

      CHKERR VecZeroEntries(common_data_simple_contact->contactAreaVec);

      CHKERR DMoFEMLoopFiniteElements(dm, "CONTACT_ELEM",
                                      fe_post_proc_simple_contact);

      CHKERR VecAssemblyBegin(common_data_simple_contact->contactAreaVec);
      CHKERR VecAssemblyEnd(common_data_simple_contact->contactAreaVec);

      const double *array;
      CHKERR VecGetArrayRead(common_data_simple_contact->contactAreaVec,
                             &array);
      if (m_field.get_comm_rank() == 0) {
        PetscPrintf(PETSC_COMM_SELF, "Active area: %8.8f out of %8.8f\n",
                    array[0], array[1]);
      }
      CHKERR VecRestoreArrayRead(common_data_simple_contact->contactAreaVec,
                                 &array);

      if (print_energy) {
        Vec v_energy;
        CHKERR HookeElement::calculateEnergy(
            dm, block_sets_ptr, "SPATIAL_POSITION", "MESH_NODE_POSITIONS",
            false, false, &v_energy);
        const double *eng_ptr;
        CHKERR VecGetArrayRead(v_energy, &eng_ptr);
        // Print elastic energy
        PetscPrintf(PETSC_COMM_WORLD, "Elastic energy: %8.8e\n", *eng_ptr);
      }

      CHKERR DMoFEMLoopFiniteElements(dm, "CONTACT_POST_PROC",
                                      post_proc_contact_ptr);

      {
        string out_file_name;
        std::ostringstream stm;
        stm << "out_contact_" << ss << ".h5m";
        out_file_name = stm.str();
        CHKERR PetscPrintf(PETSC_COMM_WORLD, "Write file %s\n",
                           out_file_name.c_str());
        CHKERR post_proc_contact_ptr->postProcMesh.write_file(
            out_file_name.c_str(), "MOAB", "PARALLEL=WRITE_PART");
      }

      if (out_integ_pts) {
        string out_file_name;
        std::ostringstream strm;
        strm << "out_contact_integ_pts_" << ss << ".h5m";
        out_file_name = strm.str();
        CHKERR PetscPrintf(PETSC_COMM_WORLD, "Write file %s\n",
                           out_file_name.c_str());
        CHKERR mb_post.write_file(out_file_name.c_str(), "MOAB",
                                  "PARALLEL=WRITE_PART");
      }

      if (out_volume) {
        PetscPrintf(PETSC_COMM_WORLD, "Loop post proc in the volume\n");
        CHKERR DMoFEMLoopFiniteElements(dm, "ELASTIC", &post_proc);

        string out_file_name;
        std::ostringstream stm;
        stm << "out_" << ss << ".h5m";
        out_file_name = stm.str();
        PetscPrintf(PETSC_COMM_WORLD, "Write file %s\n", out_file_name.c_str());
        CHKERR post_proc.writeFile(out_file_name.c_str());
      }

      if (out_skin) {
        PetscPrintf(PETSC_COMM_WORLD, "Loop post proc on the skin\n");
        CHKERR DMoFEMLoopFiniteElements(dm, "SKIN", &post_proc_skin);
        ostringstream stm;
        string out_file_name;
        stm << "out_skin_" << ss << ".h5m";
        out_file_name = stm.str();
        PetscPrintf(PETSC_COMM_WORLD, "Write file %s\n", out_file_name.c_str());
        CHKERR post_proc_skin.writeFile(stm.str());
      }
    }
  }
  CATCH_ERRORS;

  // finish work cleaning memory, getting statistics, etc
  MoFEM::Core::Finalize();

  return 0;
}