/** \file create_rough_interface.cpp
  \example create_rough_interface.cpp
  \brief Create rough surface, add prism interface and partition the mesh
*/

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#include <MoFEM.hpp>

namespace bio = boost::iostreams;
using bio::stream;
using bio::tee_device;

using namespace MoFEM;

static char help[] = "...\n\n";
static int debug = 1;

int main(int argc, char *argv[]) {

  MoFEM::Core::Initialize(&argc, &argv, (char *)0, help);

  try {

    moab::Core mb_instance;
    moab::Interface &moab = mb_instance;
    int rank;
    MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

    // Read parameters from line command
    PetscBool flg_mesh_file = PETSC_TRUE;
    PetscBool flg_out_file = PETSC_TRUE;
    PetscBool flg_data_file = PETSC_TRUE;
    char mesh_file_name[255];
    char out_file_name[255] = "out.h5m";
    char data_file_name[255];

    int nb_side_nodes = 1;
    double length_scale = 1.0;
    double mesh_height = 1.0;

    int adj_dim = 2;
    int n_parts = 1;
    int fixed_bc_block_id = 10;
    int skin_block_id = 20;
    int contact_weight = 10;

    CHKERR PetscOptionsBegin(PETSC_COMM_WORLD, "", "ADD_PRISMS_LAYER", "none");

    CHKERR PetscOptionsString("-my_file", "mesh file name", "", "mesh.h5m",
                              mesh_file_name, 255, &flg_mesh_file);
    CHKERR PetscOptionsString("-my_out_file", "out file name", "", "out.h5m",
                              out_file_name, 255, &flg_out_file);
    CHKERR PetscOptionsString("-my_data_file", "data file name", "", "data.txt",
                              data_file_name, 255, &flg_data_file);

    CHKERR PetscOptionsInt("-my_side_nodes_num", "number of nodes on each side",
                           "", nb_side_nodes, &nb_side_nodes, PETSC_NULL);
    CHKERR PetscOptionsReal("-my_length_scale", "side length", "", length_scale,
                            &length_scale, PETSC_NULL);
    CHKERR PetscOptionsReal("-my_mesh_height", "vertical size of the mesh", "",
                            mesh_height, &mesh_height, PETSC_NULL);

    CHKERR PetscOptionsInt("-my_nparts", "Number of mesh parts", "", n_parts,
                           &n_parts, PETSC_NULL);
    CHKERR PetscOptionsInt("-my_contact_weight",
                           "Weight of contact tets for partitioning", "",
                           contact_weight, &contact_weight, PETSC_NULL);
    CHKERR PetscOptionsInt("-my_adj_dim",
                           "Dimension of adjacency for partitioning", "",
                           adj_dim, &adj_dim, PETSC_NULL);
    CHKERR PetscOptionsInt("-my_fixed_bc_block_id",
                           "block id for fixed Dirichlet bc", "",
                           fixed_bc_block_id, &fixed_bc_block_id, PETSC_NULL);
    CHKERR PetscOptionsInt("-my_skin_block_id", "block id for the skin", "",
                           skin_block_id, &skin_block_id, PETSC_NULL);

    int ierr = PetscOptionsEnd();
    CHKERRG(ierr);

    std::ifstream infile(data_file_name, std::ios::in);
    std::vector<double> surface_data;
    std::vector<bool> surface_check;

    if (!infile.is_open()) {
      SETERRQ(PETSC_COMM_SELF, MOFEM_NOT_FOUND,
              "could not open surface data file");
    }

    std::string line;
    while (std::getline(infile, line)) {
      std::istringstream iss(line);
      double num = 0.0;
      if (iss >> num)
        surface_data.push_back(num);
      else
        continue;
    }

    double max_surf_height =
        *std::max_element(std::begin(surface_data), std::end(surface_data));
    for (auto &node_data : surface_data) {
      node_data -= max_surf_height;
    }

    surface_check.resize(surface_data.size());
    std::fill(surface_check.begin(), surface_check.end(), false);

    if (flg_mesh_file != PETSC_TRUE) {
      SETERRQ(PETSC_COMM_SELF, 1, "*** ERROR -my_file (MESH FILE NEEDED)");
    }

    // Read mesh to MOAB
    const char *option;
    option = ""; //"PARALLEL=BCAST;";//;DEBUG_IO";
    CHKERR moab.load_file(mesh_file_name, 0, option);
    ParallelComm *pcomm = ParallelComm::get_pcomm(&moab, MYPCOMM_INDEX);
    if (pcomm == NULL)
      pcomm = new ParallelComm(&moab, PETSC_COMM_WORLD);

    MoFEM::Core core(moab);
    MoFEM::Interface &m_field = core;

    CHKERR PetscPrintf(PETSC_COMM_WORLD,
                       "Projecting surface data on the mesh\n");

    Range all_tets, all_nodes;
    for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field, BLOCKSET, bit)) {
      if (bit->getName().compare(0, 11, "MAT_ELASTIC") == 0) {
        Range tets;
        CHKERR moab.get_entities_by_dimension(bit->getMeshset(), 3, tets, true);
        all_tets.merge(tets);
      }
    }
    CHKERR moab.get_connectivity(all_tets, all_nodes);

    for (Range::iterator nit = all_nodes.begin(); nit != all_nodes.end();
         nit++) {
      double coords[3];
      CHKERR moab.get_coords(&*nit, 1, coords);
      for (int i : {0, 1, 2}) {
        coords[i] *= length_scale;
      }
      CHKERR moab.set_coords(&*nit, 1, coords);
    }
    double scaled_height = mesh_height * length_scale;
    double edge_length = length_scale / (double)(nb_side_nodes - 1);
    double tol = 1e-12;

    auto flat_ind = [&](int i, int j) { return i * nb_side_nodes + j; };

    for (Range::iterator nit = all_nodes.begin(); nit != all_nodes.end();
         nit++) {
      double coords[3];
      CHKERR moab.get_coords(&*nit, 1, coords);
      double x = coords[0];
      double y = coords[1];
      double z = coords[2];
      double height_coef = (scaled_height + z) / scaled_height;
      double ksi, eta;
      double pos_x = x / edge_length;
      double pos_y = y / edge_length;

      double pos_x_round = std::round(pos_x);
      double pos_y_round = std::round(pos_y);
     
      if (std::abs(pos_x_round - pos_x) < tol) {
        pos_x = pos_x_round;
      }
      if (std::abs(pos_y_round - pos_y) < tol) {
        pos_y = pos_y_round;
      }

      int i, j, i_next, j_next;
      i = i_next = (int)std::floor(pos_x);
      j = j_next = (int)std::floor(pos_y);

      if (std::abs(pos_x - std::floor(pos_x)) < tol) {
        ksi = -1.0;
      } else {
        ksi = (pos_x - std::floor(pos_x)) * 2.0 - 1.0;
        if (ksi < -1. - tol || ksi > 1. + tol) {
          SETERRQ2(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                   "Wrong ksi for i = %d, j = %d", i, j);
        }
        i_next++;
      }
      if (std::abs(pos_y - std::floor(pos_y)) < tol) {
        eta = -1.0;
      } else {
        eta = (pos_y - std::floor(pos_y)) * 2.0 - 1.0;
        if (eta < -1. - tol || eta > 1. + tol) {
          SETERRQ2(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                   "Wrong eta for i = %d, j = %d", i, j);
        }
        j_next++;
      }
      if (std::abs(z) < tol) {
        if (std::abs(pos_x - std::floor(pos_x)) > tol ||
            std::abs(pos_y - std::floor(pos_y)) > tol) {
          SETERRQ2(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                   "Cannot find data for surface node i = %d, j = %d", i, j);
        } else {
          if (surface_check[flat_ind(i, j)]) {
            SETERRQ2(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                     "Data used twice for surface node i = %d, j = %d", i, j);
          } else {
            surface_check[flat_ind(i, j)] = true;
          }
        }
      }

      std::array<double, 4> shape_quad;
      shape_quad[0] = 0.25 * (1.0 - ksi) * (1.0 - eta);
      shape_quad[1] = 0.25 * (1.0 + ksi) * (1.0 - eta);
      shape_quad[2] = 0.25 * (1.0 + ksi) * (1.0 + eta);
      shape_quad[3] = 0.25 * (1.0 - ksi) * (1.0 + eta);

      std::array<double, 4> gap_quad;
      gap_quad[0] = surface_data[flat_ind(i, j)];
      gap_quad[1] = surface_data[flat_ind(i_next, j)];
      gap_quad[2] = surface_data[flat_ind(i_next, j_next)];
      gap_quad[3] = surface_data[flat_ind(i, j_next)];

      for (int k : {0, 1, 2, 3}) {
        coords[2] += shape_quad[k] * gap_quad[k] * height_coef;
      }
      CHKERR moab.set_coords(&*nit, 1, coords);
    }

    for (int i = 0; i < nb_side_nodes; i++) {
      for (int j = 0; j < nb_side_nodes; j++) {
        if (!surface_check[flat_ind(i, j)]) {
          SETERRQ2(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                   "Data not used for surface node i = %d, j = %d", i, j);
        }
      }
    }

    auto set_prism_layer_thickness = [&](const Range &prisms,
                                         bool move_top_face = true) {
      MoFEMFunctionBegin;
      Range nodes;
      int ff = 4;
      if (!move_top_face)
        ff = 3;
      for (Range::iterator pit = prisms.begin(); pit != prisms.end(); pit++) {
        EntityHandle face;
        CHKERR moab.side_element(*pit, 2, ff, face);
        const EntityHandle *conn;
        int number_nodes = 0;
        CHKERR moab.get_connectivity(face, conn, number_nodes, false);
        nodes.insert(&conn[0], &conn[number_nodes]);
      }
      double coords[3], director[3];
      for (Range::iterator nit = nodes.begin(); nit != nodes.end(); nit++) {
        CHKERR moab.get_coords(&*nit, 1, coords);
        if (coords[2] > 0.0) {
          SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                  "Solid's surface is on the wrong side of the plane z = 0");
        }
        director[0] = 0.0;
        director[1] = 0.0;
        director[2] = -coords[2];
        cblas_daxpy(3, 1, director, 1, coords, 1);
        CHKERR moab.set_coords(&*nit, 1, coords);
      }
      MoFEMFunctionReturn(0);
    };

    CHKERR PetscPrintf(PETSC_COMM_WORLD, "Inserting prism interface\n");

    MeshsetsManager *mmanager_ptr;
    CHKERR m_field.getInterface(mmanager_ptr);

    PrismsFromSurfaceInterface *prisms_from_surface_interface;
    CHKERR m_field.getInterface(prisms_from_surface_interface);

    Range contact_prisms, bot_tris, top_tris;

    for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field, BLOCKSET, bit)) {
      if (bit->getName().compare(0, 11, "INT_CONTACT") == 0) {
        Range tris;
        CHKERR moab.get_entities_by_dimension(bit->getMeshset(), 2, tris, true);
        bot_tris.merge(tris);
      }
    }

    CHKERR prisms_from_surface_interface->createPrisms(
        bot_tris, PrismsFromSurfaceInterface::SWAP_TRI_NODE_ORDER,
        contact_prisms);

    BitRefLevel bit_level0;
    bit_level0.set(0);
    CHKERR m_field.getInterface<BitRefManager>()->setBitRefLevelByDim(
        0, 3, bit_level0);
    auto all_bits = []() { return BitRefLevel().set(); };

    CHKERR set_prism_layer_thickness(contact_prisms);

    CHKERR moab.get_adjacencies(contact_prisms, 2, false, top_tris,
                                moab::Interface::UNION);
    top_tris = top_tris.subset_by_type(MBTRI);
    top_tris = subtract(top_tris, bot_tris);
    Range edges, verts;
    CHKERR moab.get_adjacencies(top_tris, 1, false, edges,
                                moab::Interface::UNION);
    CHKERR moab.get_connectivity(top_tris, verts, true);
    top_tris.merge(edges);
    top_tris.merge(verts);

    CHKERR mmanager_ptr->addMeshset(NODESET, fixed_bc_block_id, "RIGID_FLAT");
    CHKERR mmanager_ptr->addEntitiesToMeshset(NODESET, fixed_bc_block_id,
                                              top_tris);

    DisplacementCubitBcData disp_bc;
    std::memcpy(disp_bc.data.name, "Displacement", 12);
    disp_bc.data.flag1 = 1;
    disp_bc.data.flag2 = 1;
    disp_bc.data.flag3 = 1;
    disp_bc.data.flag4 = 0;
    disp_bc.data.flag5 = 0;
    disp_bc.data.flag6 = 0;
    disp_bc.data.value1 = 0;
    disp_bc.data.value2 = 0;
    disp_bc.data.value3 = 0;
    disp_bc.data.value4 = 0;
    disp_bc.data.value5 = 0;
    disp_bc.data.value6 = 0;

    CHKERR mmanager_ptr->setBcData(NODESET, fixed_bc_block_id, disp_bc);

    Skinner skinner(&moab);
    Range skin_tris, tets_level;
    CHKERR m_field.getInterface<BitRefManager>()->getEntitiesByTypeAndRefLevel(
        bit_level0, all_bits(), MBTET, tets_level);
    CHKERR skinner.find_skin(0, tets_level, false, skin_tris);

    CHKERR mmanager_ptr->addMeshset(BLOCKSET, skin_block_id, "SKIN");
    CHKERR mmanager_ptr->addEntitiesToMeshset(BLOCKSET, skin_block_id,
                                              skin_tris);
    
    EntityHandle out_meshset_skin;
    CHKERR moab.create_meshset(MESHSET_SET, out_meshset_skin);
    CHKERR moab.add_entities(out_meshset_skin, skin_tris);

    size_t last_index = std::string(out_file_name).find_last_of(".");
    std::string skin_file_name =
        std::string(out_file_name).substr(0, last_index) + "_skin.vtk";

    CHKERR PetscPrintf(PETSC_COMM_WORLD, "Writing file %s\n",
                       skin_file_name.c_str());
    CHKERR moab.write_file(skin_file_name.c_str(), "VTK", "", &out_meshset_skin,
                           1);

    CHKERR moab.delete_entities(&out_meshset_skin, 1);

    CHKERR PetscPrintf(PETSC_COMM_WORLD, "Partitioning the mesh\n");

    Tag th_vertex_weight;
    int def_val = 1;
    CHKERR moab.tag_get_handle("VERTEX_WEIGHT", 1, MB_TYPE_INTEGER,
                               th_vertex_weight, MB_TAG_CREAT | MB_TAG_DENSE,
                               &def_val);

    Range bot_verts, bot_tets;
    CHKERR moab.get_connectivity(bot_tris, bot_verts, true);
    CHKERR moab.get_adjacencies(bot_verts, 3, false, bot_tets,
                                moab::Interface::UNION);
    bot_tets = bot_tets.subset_by_type(MBTET);
    std::vector<int> bot_tets_weights(bot_tets.size());
    for (int i = 0; i < bot_tets.size(); i++) {
      bot_tets_weights[i] = contact_weight;
    }
    CHKERR moab.tag_set_data(th_vertex_weight, bot_tets,
                             &*bot_tets_weights.begin());

    Range tets;
    CHKERR moab.get_entities_by_type(0, MBTET, tets, true);

    ProblemsManager *prb_mng_ptr;
    CHKERR m_field.getInterface(prb_mng_ptr);
    CHKERR prb_mng_ptr->partitionMesh(tets, 3, adj_dim, n_parts,
                                      &th_vertex_weight, NULL, NULL);

    Tag part_tag = pcomm->part_tag();

    std::vector<int> part_number(contact_prisms.size());
    Range::iterator pit = contact_prisms.begin();
    for (int ii = 0; pit != contact_prisms.end(); pit++, ii++) {
      Range adj_tris, adj_tet;
      CHKERR moab.get_adjacencies(&*pit, 1, 2, false, adj_tris,
                                  moab::Interface::UNION);
      adj_tris = adj_tris.subset_by_type(MBTRI);
      CHKERR moab.get_adjacencies(adj_tris, 3, false, adj_tet,
                                  moab::Interface::UNION);
      adj_tet = adj_tet.subset_by_type(MBTET);
      int part_num;
      CHKERR moab.tag_get_data(part_tag, adj_tet, &part_num);
      CHKERR moab.tag_set_data(part_tag, &*pit, 1, &part_num);
      part_number[ii] = part_num;
    }

    auto get_gid_tag = [&]() {
      Tag gid_tag;
      rval = moab.tag_get_handle(GLOBAL_ID_TAG_NAME, gid_tag);
      if (rval != MB_SUCCESS) {
        const int zero = 0;
        CHKERR moab.tag_get_handle(GLOBAL_ID_TAG_NAME, 1, MB_TYPE_INTEGER,
                                   gid_tag, MB_TAG_DENSE | MB_TAG_CREAT, &zero);
      }
      return gid_tag;
    };
    Tag gid_tag = get_gid_tag();

    std::map<int, Range> parts_ents;
    int dim = 3;

    // get entities on each part
    pit = contact_prisms.begin();
    for (int ii = 0; pit != contact_prisms.end(); pit++, ii++) {
      parts_ents[part_number[ii]].insert(*pit);
    }
    Range tagged_sets;
    CHKERR moab.get_entities_by_type_and_tag(0, MBENTITYSET, &part_tag, NULL, 1,
                                             tagged_sets,
                                             moab::Interface::UNION);

    // get lower dimension entities on each part
    for (int pp = 0; pp != n_parts; pp++) {
      Range dim_ents = parts_ents[pp].subset_by_dimension(dim);
      for (int dd = dim - 1; dd != -1; dd--) {
        Range adj_ents;
        CHKERR moab.get_adjacencies(dim_ents, dd, true, adj_ents,
                                    moab::Interface::UNION);
        parts_ents[pp].merge(adj_ents);
      }
    }
    for (int pp = 1; pp != n_parts; pp++) {
      for (int ppp = 0; ppp != pp; ppp++) {
        parts_ents[pp] = subtract(parts_ents[pp], parts_ents[ppp]);
      }
    }

    for (int rr = 0; rr != n_parts; rr++) {
      if (!parts_ents[rr].size()) {
        SETERRQ1(PETSC_COMM_SELF, MOFEM_OPERATION_UNSUCCESSFUL,
                 "Part %d has no prism elements, try increasing the contact "
                 "weight parameter",
                 rr);
      }
    }

    for (int pp = 0; pp != n_parts; pp++) {
      CHKERR moab.add_entities(tagged_sets[pp], parts_ents[pp]);
    }

    // set gid to lower dimension entities
    for (int dd = 0; dd <= dim; dd++) {
      int gid = 1; // moab indexing from 1
      for (int pp = 0; pp != n_parts; pp++) {
        Range dim_ents = parts_ents[pp].subset_by_dimension(dd);
        for (Range::iterator eit = dim_ents.begin(); eit != dim_ents.end();
             eit++) {
          if (dd > 0) {
            CHKERR moab.tag_set_data(part_tag, &*eit, 1, &pp);
          }
          CHKERR moab.tag_set_data(gid_tag, &*eit, 1, &gid);
          gid++;
        }
      }
    }

    CHKERR PetscPrintf(PETSC_COMM_WORLD, "Writing file %s\n", out_file_name);
    CHKERR moab.write_file(out_file_name);
  }
  CATCH_ERRORS;

  MoFEM::Core::Finalize();
  return 0;
}
